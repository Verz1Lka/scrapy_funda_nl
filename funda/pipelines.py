# -*- coding: utf-8 -*-
from scrapy.exporters import CsvItemExporter
from scrapy import signals
from funda.items import FundaEmployeesItem, FundaAgentItem, FundaItem
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html



class FundaPipeline(object):

    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def __init__(self):
        self.file_objects = open('objects.csv', 'w')
        self.file_agents = open('agents.csv', 'w')
        self.file_employees = open('employees.csv', 'w')
        self.exporter_objects = CsvItemExporter(self.file_objects)
        self.exporter_agents = CsvItemExporter(self.file_agents)
        self.exporter_employees = CsvItemExporter(self.file_employees)

    def spider_opened(self, spider):
        self.exporter_objects.start_exporting()
        self.exporter_agents.start_exporting()
        self.exporter_employees.start_exporting()

    def spider_closed(self, spider):
        self.exporter_objects.finish_exporting()
        self.exporter_agents.finish_exporting()
        self.exporter_employees.finish_exporting()
        self.file_objects.close()
        self.file_agents.close()
        self.file_employees.close()

    def process_item(self, item, spider):
        if isinstance(item, FundaItem):
            exporter = self.exporter_objects
        elif isinstance(item, FundaAgentItem):
            exporter = self.exporter_agents
        elif isinstance(item, FundaEmployeesItem):
            exporter = self.exporter_employees
        else:
            return item
        exporter.export_item(item)
        return item

