# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FundaItem(scrapy.Item):
    # define the fields for your item here like:
    state = scrapy.Field()
    city = scrapy.Field()
    neighborhood = scrapy.Field()
    street = scrapy.Field()
    image_url = scrapy.Field()
    zipcode = scrapy.Field()
    price = scrapy.Field()
    delisting_date = scrapy.Field()

    last_asking_price = scrapy.Field()
    date_listed = scrapy.Field()
    term = scrapy.Field()
    date_sold = scrapy.Field()
    status = scrapy.Field()

    building_type = scrapy.Field()
    building_type_details = scrapy.Field()
    build_year = scrapy.Field()
    living_area = scrapy.Field()
    rooms_count = scrapy.Field()
    asking_price = scrapy.Field()

    sold = scrapy.Field()

    path = scrapy.Field()
    buying_agent_id = scrapy.Field()
    selling_agent_id = scrapy.Field()

class FundaAgentItem(scrapy.Item):
    name = scrapy.Field()
    phone = scrapy.Field()
    street = scrapy.Field()
    zipcode = scrapy.Field()
    city = scrapy.Field()
    employees_amount = scrapy.Field()
    website_url = scrapy.Field()

    picture_url = scrapy.Field()
    description = scrapy.Field()
    path = scrapy.Field()

    brokers_association = scrapy.Field()

    tagline = scrapy.Field()


class FundaEmployeesItem(scrapy.Item):
    name = scrapy.Field()
    jobtitle = scrapy.Field()
    phone = scrapy.Field()
    agent_id = scrapy.Field()