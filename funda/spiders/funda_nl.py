# -*- coding: utf-8 -*-
import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join
from funda.items import FundaItem, FundaAgentItem, FundaEmployeesItem


class FundaNlSpider(scrapy.Spider):
    name = "funda.nl"
    allowed_domains = ["funda.nl"]
    start_urls = (
        'http://www.funda.nl/koop/verkocht/heel-nederland/',
    )

    def start_requests(self):
        rq = scrapy.Request(url='http://www.funda.nl/koop/verkocht/heel-nederland/', callback=self.parse_sold)
        yield rq
        rq = scrapy.Request(url='http://www.funda.nl/koop/heel-nederland/', callback=self.parse_sale)
        yield rq
        return

    def parse_sold(self, response):
        for link in response.xpath('//h3/a[@class="object-street "]/@href').extract():
            rq = scrapy.Request(response.urljoin(link), callback=self.get_prep_sold_object)
            yield rq
        for row in response.xpath('//ul[@class="link-list paging-list"]/li/a'):
            next_page = row.xpath('./@href').extract()[0]
            rq = scrapy.Request(response.urljoin(next_page), callback=self.parse_sold)
            yield rq
        return

    def get_prep_sold_object(self, response):
        link = response.xpath('//ul[@class="tabbed-list"]/li/a[text()="Kenmerken"]/@href').extract()
        if link:
            rq = scrapy.Request(response.urljoin(link[0]), callback=self.get_sold_object)
            yield rq
            return

        l = FundaNlLoader(item=FundaItem(), response=response)
        l.add_value('sold', '1')
        l.add_value('path', response.url)
        l.add_xpath('state', '//p[@class="section path-nav"]/span[3]/a/span/text()')
        l.add_xpath('city', '//p[@class="section path-nav"]/span[4]/a/span/text()')
        l.add_xpath('neighborhood', '//p[@class="section path-nav"]/span[5]/a/span/text()')
        l.add_xpath('street', '//h1/text()')
        l.add_xpath('image_url', '//div[@class="prop-hdr sold"]/a/img/@src')
        l.add_xpath('zipcode', '//div[@class="prop-hdr sold"]/p[1]/text()')
        l.add_xpath('price', '//span[@class="price-wrapper"]/span/text()')
        l.add_xpath('delisting_date', '//span[@class="txt-sft"]/text()')

        l.add_xpath('date_listed', '//span[@class="transaction-date"][contains(text(),"Aangeboden sinds")]/'
                                   'strong/text()')
        l.add_xpath('term', '//span[@class="transaction-date"][contains(text(),"Looptijd")]/strong/text()')
        l.add_xpath('date_sold', '//span[@class="transaction-date lst"][contains(text(),"Verkoopdatum")]/strong/text()')

        l.add_xpath('building_type', '//table[contains(@class,"specs specs-cats")]/tr[1]/th/text()')
        l.add_xpath('building_type_details', '//table[contains(@class,"specs specs-cats")]/tr[1]/td/span/text()')
        l.add_xpath('build_year', '//table[contains(@class,"specs specs-cats")]//tr[th[text()="Bouwjaar"]]'
                                  '/td/span/text()')
        l.add_xpath('living_area', '//table[contains(@class,"specs specs-cats")]//tr[th[text()="Oppervlakte"]]/'
                                   'td/span/text()')
        l.add_xpath('rooms_count', '//table[contains(@class,"specs specs-cats")]//tr[th[text()="Aantal kamers"]]/'
                                   'td/span/text()')
        l.add_xpath('status', '//h1/span/text()')

        agent_links = response.xpath('//div[@class="rel-hdr"]/div[@class="rel-info"]/h3/a/@href').extract()
        buying_agent_id = '0'
        selling_agent_id = '0'
        if len(agent_links) > 0:
            selling_agent_id = agent_links[0].split('/')[-2].split('-')[0]
            rq = scrapy.Request(url=response.urljoin(agent_links[0]), callback=self.get_not_nvm_agent)
            broker_association = ' '.join(response.xpath('//div[contains(@class,"wbb rel")][./h2]/@class')
                                          .extract()).split()[-1]
            rq.meta['broker_association'] = broker_association
            yield rq

        l.add_value('buying_agent_id', buying_agent_id)
        l.add_value('selling_agent_id', selling_agent_id)
        yield l.load_item()
        return

    def get_sold_object(self, response):
        l = FundaNlLoader(item=FundaItem(), response=response)
        l.add_value('sold', '1')
        l.add_value('path', response.url)
        l.add_xpath('state', '//p[@class="section path-nav"]/span[3]/a/span/text()')
        l.add_xpath('city', '//p[@class="section path-nav"]/span[4]/a/span/text()')
        l.add_xpath('neighborhood', '//p[@class="section path-nav"]/span[5]/a/span/text()')
        l.add_xpath('street', '//h1/text()')
        l.add_xpath('image_url', '//div[@class="prop-hdr sold"]/a/img/@src')
        l.add_xpath('zipcode', '//div[@class="prop-hdr sold"]/p[1]/text()')
        l.add_xpath('price', '//span[@class="price-wrapper"]/span/text()')
        l.add_xpath('delisting_date', '//span[@class="txt-sft"]/text()')

        l.add_xpath('last_asking_price', '//table[@class="specs specs-cats"]//tr[th[text()="Laatste vraagprijs"]]/'
                                         'td/span/span/text()')
        l.add_xpath('date_listed', '//table[@class="specs specs-cats"]//tr[th[text()="Aangeboden sinds"]]/'
                                   'td/span/text()')
        l.add_xpath('term', '//table[@class="specs specs-cats"]//tr[th[text()="Looptijd"]]/td/span/text()')
        l.add_xpath('date_sold', '//table[@class="specs specs-cats"]//tr[th[text()="Verkoopdatum"]]/td/span/text()')
        l.add_xpath('status', '//table[@class="specs specs-cats"]//tr[th[text()="Status"]]/td/span/span/text()')
        l.add_xpath('building_type', '//table[@class="specs specs-cats"]/tr[th[contains(text(),"Bouw")]]/'
                                     'following-sibling::tr[1]/th/text()')
        l.add_xpath('building_type_details', '//table[@class="specs specs-cats"]/tr[th[contains(text(),"Bouw")]]/'
                                             'following-sibling::tr[1]/td/span/text()')
        l.add_xpath('build_year', '//table[@class="specs specs-cats"]//tr[th[text()="Bouwjaar"]]/td/span/text()')
        l.add_xpath('living_area', '//table[@class="specs specs-cats"]//tr[th[text()="Wonen (= woonoppervlakte) "]]/'
                                   'td/span/text()')
        l.add_xpath('rooms_count', '//table[@class="specs specs-cats"]//tr[th[text()="Aantal kamers"]]/td/span/text()')

        broker_association = response.xpath('//div[contains(@class,"wbb rel")][./h2]/@class').extract()
        agent_links = response.xpath('//div[@class="rel-hdr"]/div[@class="rel-info"]/h3/a/@href').extract()

        buying_agent_id = '0'
        selling_agent_id = '0'
        if len(agent_links) > 0:
            ba = broker_association[0].split()[-1]
            if ba == 'nvm':
                cb = self.get_sale_agent
            else:
                cb = self.get_not_nvm_agent
            selling_agent_id = agent_links[0].split('/')[-2].split('-')[0]
            rq = scrapy.Request(url=response.urljoin(agent_links[0]), callback=cb)
            rq.meta['broker_association'] = ba
            yield rq
        if len(agent_links) > 1:
            ba = broker_association[1].split()[-1]
            if ba == 'nvm':
                cb = self.get_sale_agent
            else:
                cb = self.get_not_nvm_agent
            buying_agent_id = agent_links[1].split('/')[-2].split('-')[0]
            rq = scrapy.Request(url=response.urljoin(agent_links[1]), callback=cb)
            rq.meta['broker_association'] = ba
            yield rq

        l.add_value('buying_agent_id', buying_agent_id)
        l.add_value('selling_agent_id', selling_agent_id)
        l.add_xpath('brokers_association', broker_association[0].split()[-1])
        yield l.load_item()

        return

    def parse_sale(self, response):
        for link in response.xpath('//div[@class="search-result-header"]/a/@href').extract():
            rq = scrapy.Request(response.urljoin(link), callback=self.get_sale_object)
            yield rq
        for row in response.xpath('//ul[@class="pagination"]/li[@class="page-number"]/a'):
            next_page = row.xpath('./@href').extract()[0]
            rq = scrapy.Request(response.urljoin(next_page), callback=self.parse_sale)
            yield rq
        return

    def get_sale_object(self, response):
        l = FundaNlLoader(item=FundaItem(), response=response)
        l.add_value('sold', '0')
        l.add_value('path', response.url)
        l.add_xpath('city', '//div[@class="breadcrumb"]/ol/li[2]/a/text()')
        l.add_xpath('neighborhood', '//div[@class="breadcrumb"]/ol/li[3]/a/text()')
        l.add_xpath('street', '//h1[@class="object-header-title"]/text()')
        l.add_xpath('zipcode', '//h1/span/text()')
        l.add_xpath('price', '//div[@class="object-header-pricing"]/strong/text()')
        l.add_xpath('image_url', '//div[@class="object-media-foto"]/a/@href | '
                                 '//div[@class="object-header-image"]/img/@src')

        l.add_xpath('asking_price', '//dl[@class="object-kenmerken-list"]/dt[text()="Vraagprijs"]/'
                                    'following-sibling::dd[1]/text()')
        l.add_xpath('date_listed', '//dl[@class="object-kenmerken-list"]/dt[text()="Aangeboden sinds"]/'
                                   'following-sibling::dd[1]/text()')
        l.add_xpath('status', '//dl[@class="object-kenmerken-list"]/dt[text()="Status"]/'
                              'following-sibling::dd[1]/text() | '
                              '//ul[@class="labels"]/li[@class="label-transactie-voorbehoud"]/text()')
        l.add_xpath('building_type', '//div[@class="object-kenmerken-body"]/dl[2]/dt[1]/text()| '
                                     '//dl[@class="object-kenmerken-list"]/dt[3]/text()')
        l.add_xpath('building_type_details', '//div[@class="object-kenmerken-body"]/dl[2]/dd[1]/text()| '
                                             '//dl[@class="object-kenmerken-list"]/dd[3]/text()')
        l.add_xpath('build_year', '//div[@class="object-kenmerken-body"]/dl/dt[text()="Bouwjaar"]/'
                                  'following-sibling::dd[1]/text()')
        l.add_xpath('living_area', '//div[@class="object-kenmerken-body"]/dl//dt[text()="Woonoppervlakte"]/'
                                   'following-sibling::dd[1]/text() | '
                                   '//dt[text()="Oppervlakte"]/following-sibling::dd[1]/text()')
        l.add_xpath('rooms_count', '//div[@class="object-kenmerken-body"]/dl//dt[text()="Aantal kamers"]/'
                                   'following-sibling::dd[1]/text()')

        agent_id = '0'
        agent_links = response.xpath('//h2[@class="object-contact-aanbieder-name"]')
        for ag_link in agent_links:
            if len(agent_links) == 1:
                bat = ' '.join(response.xpath('//h2[@class="object-contact-aanbieder-name"]/span/@title'
                                             ).extract()).split()
                if bat:
                    ba = bat[0]
                else:
                    ba = ' '.join(response.xpath('//section[@class="object-contact"]/div/@class').extract()).split()[-1]
            else:
                ba = ag_link.xpath('./span/@class').extract()[0].split()[-1].split('-')[-1]
            if ba == 'nvm':
                cb = self.get_sale_agent
            else:
                cb = self.get_not_nvm_agent
            agent_id = ag_link.xpath('./a/@href').extract()[0].split('/')[-2].split('-')[0]
            rq = scrapy.Request(url=response.urljoin(ag_link.xpath('./a/@href').extract()[0]), callback=cb)
            rq.meta['broker_association'] = ba
            yield rq
        l.add_value('selling_agent_id', agent_id)

        yield l.load_item()

        return

    def get_sale_agent(self, response):
        l = FundaAgentLoader(item=FundaAgentItem(), response=response)
        l.add_value('path', response.url)
        l.add_xpath('name', '//h1[@itemprop="name"]/text()')
        l.add_xpath('tagline', '//section[@class="makelaars-header"]/p/text()')
        l.add_xpath('picture_url', '//section[@class="makelaars-header"]/div[@class="makelaars-header-logo"]/img/@src')
        l.add_xpath('street', '//div[@class="object-contact-text-content"]/text()')
        l.add_xpath('zipcode', '//div[@class="object-contact-text-content"]/text()')
        l.add_xpath('city', '//div[@class="object-contact-text-content"]/text()')
        l.add_xpath('phone', '//div[@class="object-contact-react-phone"]/div/text()')
        l.add_xpath('description', '//div[@class="object-description-body"]//text()')

        l.add_xpath('employees_amount', '//section[@class="makelaars-collaborators"]/a/text()')
        l.add_xpath('website_url', '//a[@class="makelaars-contact-website"]/@href')

        l.add_value('brokers_association', response.meta['broker_association'])

        yield l.load_item()

        rq = scrapy.Request(url = response.url + 'onze-medewerkers/', callback=self.get_employees)
        yield rq
        return

    def get_not_nvm_agent(self, response):
        l = FundaAgentLoader(item=FundaAgentItem(), response=response)
        l.add_value('path', response.url)

        l.add_xpath('street', '//div[@itemprop="address"]/span[@itemprop="streetAddress"]/text()')
        l.add_xpath('zipcode', '//div[@itemprop="address"]/span[@itemprop="postalCode"]/text()')
        l.add_xpath('city', '//div[@itemprop="address"]/span[@itemprop="addressLocality"]/text()')
        l.add_xpath('phone', '//a[@itemprop="telephone"]/text()')
        l.add_xpath('website_url', '//a[@id="MakelaarWebsiteLink"]/@href')

        l.add_xpath('name', '//h1[@itemprop="name"]/text()')
        l.add_value('brokers_association', response.meta['broker_association'])

        yield l.load_item()

        return

    def get_employees(self, response):
        for row in response.xpath('//section[@class="makelaars-collaborators-list"]/ul/li'):
            l = FundaEmployeesLoader(item=FundaEmployeesItem(), selector=row)
            l.add_xpath('name', './/div[@class="makelaars-collaborators-list-info"]/h3/text()')
            l.add_xpath('jobtitle', './/div[@class="makelaars-collaborators-list-info"]/'
                                'div[@class="makelaars-collaborators-list-jobtitle"]/text()')
            l.add_xpath('phone', './/div[@class="makelaars-collaborators-list-info"]/ul/li/a/text()')
            l.add_value('agent_id', response.url.split('/')[-3].split('-')[0])
            yield l.load_item()
        return


class FundaNlLoader(ItemLoader):
    def remove_trash(value):
        return ' '.join(value.split())

    def fix_asking_price(value):
        if value.split():
            return value.split()[1]
        return value

    default_output_processor = TakeFirst()
    default_input_processor = MapCompose(remove_trash)
    delisting_date_in = MapCompose(lambda value: value.split()[-1])
    price_in = MapCompose(lambda value: value.split()[-1])
    zipcode_in = MapCompose(lambda value: ' '.join(value.split()[:2]))
    asking_price_in = MapCompose(fix_asking_price)
    last_asking_price_in = MapCompose(fix_asking_price)
    brokers_association_in = MapCompose(lambda value: value.split()[-1])


class FundaAgentLoader(ItemLoader):
    def remove_trash(value):
        return ' '.join(value.split())

    def get_second(self, value):
        if len(value) > 1:
            return value[1]
        return value[0]
    def fix_zipcode(value):
        if len(value.split()) > 2:
            return ' '.join(value.split()[:2])
        return value

    def fix_city(value):
        if len(value.split()) > 2:
            return ' '.join(value.split()[2:])
        return value

    def fix_employees_amount(value):
        i0 = value.find('(') + 1
        i1 = value.find(')', i0)
        return value[i0:i1]

    default_output_processor = TakeFirst()
    default_input_processor = MapCompose(remove_trash)
    description_out = Join(' ')
    zipcode_out = get_second
    zipcode_in = MapCompose(fix_zipcode)
    street_in = MapCompose(remove_trash, lambda value: value.replace(',', ''))
    city_out = get_second
    city_in = MapCompose(fix_city)
    employees_amount_in = MapCompose(fix_employees_amount)
    phone_in = MapCompose(lambda value: value.replace(u'&laquo Telefoon:', ''), remove_trash)


class FundaEmployeesLoader(ItemLoader):
    def remove_trash(value):
        return ' '.join(value.split())

    default_output_processor = TakeFirst()
    default_input_processor = MapCompose(remove_trash)
    phone_out = Join(' / ')
